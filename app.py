import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import boto3, pickle, yaml
import numpy as np

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
from sklearn.datasets import make_classification
from sklearn.model_selection import GridSearchCV, TimeSeriesSplit
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.ensemble import RandomForestClassifier

def cargar_datos_s3(bucket, bucket_path):
  session = boto3.Session(
    aws_access_key_id = config['s3']['aws_access_key_id'],
    aws_secret_access_key = config['s3']['aws_secret_access_key'],
    aws_session_token = config['s3']['aws_session_token']
  )
  s3 = session.resource('s3')
  obj = s3.Object(bucket, bucket_path).get()['Body'].read()
  dataset = pickle.loads(obj)
  return dataset


with open('credentials.yaml', 'r') as f:
  config = yaml.safe_load(f)
session = boto3.Session(
    aws_access_key_id = config['s3']['aws_access_key_id'],
    aws_secret_access_key = config['s3']['aws_secret_access_key'],
    aws_session_token = config['s3']['aws_session_token']    
  )
s3 = session.client('s3')
bucket = "mcdia0098"
key = "feature-matrix/"
bucket_path = s3.list_objects_v2(Bucket=bucket, Prefix=key)['Contents'][-1]['Key']
dataset = cargar_datos_s3(bucket, bucket_path)

def transform_results(data):
  data['results'] = data['results'].apply(lambda i: 1 if i == 'Pass' else 0)
  return data

dataset = dataset.sort_values(['year', 'month', 'day_of_month' ])
np.random.seed(20201122)
X = dataset.drop(['results'], axis=1)
y = dataset['results']

test_size = int(round(X.shape[0] * .30))
train_size = X.shape[0] - test_size
X_train = X.iloc[0:X.shape[0] - test_size]
X_test = X.iloc[X.shape[0] - test_size:]
y_train = y.iloc[0:y.shape[0] - test_size]
y_test = y.iloc[y.shape[0] - test_size:]
dt = RandomForestClassifier()
grid = {'n_estimators': [300, 500, 800, 1300], 'max_depth':[5,7,9,13], 'min_samples_leaf': [5,9,11]}
ts = TimeSeriesSplit(n_splits=5)
gs = GridSearchCV(dt, param_grid=grid, cv=ts, scoring="precision")
gs.fit(X_train, y_train)
gs.best_estimator_

predicted_labels = gs.predict(X_test)
predicted_score = gs.predict_proba(X_test)

fpr, tpr, thresholds = roc_curve(y_test, predicted_score[:,1], pos_label=1)

#3.Incluye en tu dashboard una gráfica que muestra la curva ROC obtenida para el último dataset utilizado para la predicción.

fig = px.area(
    x=fpr, y=tpr,
    title=f'ROC Curve (AUC={auc(fpr, tpr):.4f})',
    labels=dict(x='False Positive Rate', y='True Positive Rate'),
    width=700, height=500
)
fig.add_shape(
    type='line', line=dict(dash='dash'),
    x0=0, x1=1, y0=0, y1=1
)

fig.update_yaxes(scaleanchor="x", scaleratio=1)
fig.update_xaxes(constrain='domain')

#1.Incluye en tu dashboard una gráfica de histograma con los scores obtenidos en tu predicción


fig_hist = px.histogram(
    x=predicted_score[:,1], color=y_test, nbins=50,
    labels=dict(color='True Labels', x='Score')
)

#2.Incluye en tu dashboard una gráfica de histograma con la distribución de las etiquetas de predicción

fig_hist_labels = px.histogram(
    x=predicted_score[:,0], color=y_test, nbins=50,
    labels=dict(color='True Labels', x='Score')
)


#-----------PARÁMETROS DE LA FIGURA-----------------------------

fig = px.area(
    x=fpr, y=tpr,
    title=f'ROC Curve (AUC={auc(fpr, tpr):.4f})',
    labels=dict(x='False Positive Rate', y='True Positive Rate'),
    width=700, height=500
)

fig.add_shape(
    type='line', line=dict(dash='dash'),
    x0=0, x1=1, y0=0, y1=1
)

fig.update_yaxes(scaleanchor="x", scaleratio=1)
fig.update_xaxes(constrain='domain')


# render-------------------------------------------------------------------
app.layout = html.Div(children=[
    html.H1(children='Aplicaciones de ciencias de datos 2'),

    html.Div(children='''Incluye en tu dashboard una gráfica de histograma con los scores obtenidos en tu predicción'''),
    dcc.Graph(
        id='example-graph',
        figure=fig_hist
    ),
    html.Div(children='''Incluye en tu dashboard una gráfica de histograma con la distribución de las etiquetas de predicción'''),
    dcc.Graph(
        id='histograma',
        figure=fig_hist_labels
    ),
    html.Div(children='''Incluye en tu dashboard una gráfica que muestra la curva ROC obtenida para el último dataset utilizado para la predicción'''),
    dcc.Graph(
        id="roc-curve",
        figure=fig
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
